#!/usr/bin/python3
"""
Calculate the CRC32 checksum of a file and output it in a LOOT friendly format

LOOT: https://loot.github.io/
"""

import os.path
import sys

from binascii import crc32


def get_pe_version(file):
    """Read EXE/DLL metadata and return version info."""
    try:
        import pefile
    except ModuleNotFoundError:
        return None

    pe_stringtable = pefile.PE(file.name).FileInfo[0].StringTable[0].entries
    file_versions = []
    for version in (b'FileVersion', b'ProductVersion'):
        file_versions += ['.'.join(pe_stringtable[version].decode().split(', '))]
    return file_versions


def get_plugin_version(file):
    """Read plugin file data and return its version. (Unimplemented.)"""
    return None


def crc32_of_file(file):
    """Return hexadecimal CRC32 hash of input file."""
    return format(crc32(file.read()), 'X')


def version_of_file(file):
    """Return version(s) of a file."""
    file_extension = os.path.splitext(file.name)[-1][1:]
    if file_extension in ('exe', 'dll'):
        return get_pe_version(file)
    elif file_extension in ('esm', 'esl', 'esp'):
        return get_plugin_version(file)
    else:
        return None


def loot_checksum_condition(path, checksum):
    """Return string compliant with LOOT’s checksum condition format.

    See https://loot-api.readthedocs.io/en/latest/metadata/conditions.html
    """
    return 'checksum("{path}", {checksum})'.format(
        path=path,
        checksum=checksum,
    )


def loot_version_condition(path, version, comparator='=='):
    """Return string compliant with LOOT’s version condition format.

    See https://loot-api.readthedocs.io/en/latest/metadata/conditions.html
    """

    # Test whether comparator is valid
    valid_comparators = ('==', '!=', '<', '>', '<=', '>=')
    if comparator not in valid_comparators:
        return None

    return 'version("{path}", "{version}", "{comparator}")'.format(
        path=path,
        version=version,
        comparator=comparator,
    )


def main(files):
    """Main logic operation of the script."""
    for file in files:
        if os.path.isfile(file):
            with open(file, 'rb') as f:
                checksum = crc32_of_file(f)
                print(loot_checksum_condition(f.name, checksum))
                version = version_of_file(f)
                if version is not None and len(version) > 0:
                    for v in version:
                        print(loot_version_condition(f.name, v))
        else:
            print('SKIP: {} is not a file!'.format(file))


if __name__ == '__main__':
    main(files=sys.argv[1:])

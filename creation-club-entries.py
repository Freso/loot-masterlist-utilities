#!/usr/bin/python3
"""
Parse a .ccc file and output LOOT masterlist entries

LOOT: https://loot.github.io/
"""

import os.path
import sys


def make_entries(cccfile):
    output = []
    previous_plugins = []
    with open(cccfile, 'r') as ccc:
        for plugin in ccc:
            plugin = plugin.strip()
            output += ["  - name: '{}'".format(plugin)]
            output += ['    group: *ccGroup']
            if len(previous_plugins) > 0:
                output += ['    after:']
                for after in previous_plugins:
                    output += ["      - '{}'".format(after)]
            previous_plugins += [plugin]
    return '\n'.join(output)


def main(files):
    """Main logic operation of the script."""
    for file in files:
        if os.path.isfile(file):
            print(make_entries(file))
        else:
            print('SKIP: {} is not a file!'.format(file))


if __name__ == '__main__':
    main(files=sys.argv[1:])
